global class scheduleMyBatch implements schedulable
{
    global void execute(SchedulableContext sc)
    {
    CalculateInterest si = new CalculateInterest();
      database.executebatch(si);
    }
}