declare module "@salesforce/apex/FetchAccount.getAccounts" {
  export default function getAccounts(): Promise<any>;
}
